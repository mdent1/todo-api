'use strict';

const logger = require('../../logger/logger');

const server = require('./app')({
  logger: logger
});

const current_node_env = process.env.NODE_ENV;
logger.info(`Application running with NODE_ENV = ${current_node_env}`);
server.listen(3000, '0.0.0.0', (err, address) => {
  if (err) {
    // console.log(err);
    logger.err(err);
    process.exit(1);
  }
});
