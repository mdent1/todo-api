"use strict";

require("make-promises-safe"); // installs an 'unhandledRejection' handler

const AWS = require("aws-sdk");
const logger = require("../../logger/logger");
const fastify = require("fastify");
const axios = require("axios");
const config = require("../../config/config");
const httpStatus = require("http-status-codes");
const EventEmitter = require("events");
const todoOpt = require("./option/todo_option");

const eventEmitter = new EventEmitter();
AWS.config.update({ region: config.AWS_REGION });

// Todo use case
const TodoController = require("../../usecase/todo/controller/todo_controller");
const TodoDataAdapter = require("../../usecase/todo/adapter/dynamodb_data_adapter");
const TodoInterActor = require("../../usecase/todo/interactor/todo_interactor");
const TodoEventHandler = require("../../usecase/todo/event_handler/todo_event_handler");

const documentClient = new AWS.DynamoDB.DocumentClient();
const todoDataAdapter = new TodoDataAdapter(documentClient);
const todoInterActor = new TodoInterActor(todoDataAdapter);
const todoController = new TodoController(todoInterActor, eventEmitter);
const todoEventHandler = new TodoEventHandler(todoInterActor);

function build(opts = { logger: logger }) {
  const app = fastify(opts);

  app.register(require("fastify-cors"), {
    methods: ["GET", "PUT", "POST"],
  });

  // Health check route
  app.get("/health", (request, reply) => {
    reply.code(httpStatus.OK).send({ status: "OK" });
  });

  app.post(
    "/todo",
    todoOpt,
    todoController.handleTodoRequest.bind(todoController)
  );

  app.get(
    "/todos",
    todoController.handleGetTodos.bind(todoController)
  );

  app.get(
    "/todo/:id",
    todoController.handleGetTodoById.bind(todoController)
  );

  app.put(
    "/todo/:id",
    todoOpt,
    todoController.handleUpdateTodoRequest.bind(todoController)
  );

  app.delete(
    "/todo/:id",
    todoController.handleDeleteTodo.bind(todoController)
  );

  return app;
}

module.exports = build;
