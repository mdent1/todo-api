const opts = {
    schema: {
      body: {
        type: 'object',
        required: ['title', 'description'],
        properties: {
          title: {type: 'string'},
          description: {type: 'string'},
          createdDate: {type: 'string'}
        }
      }
    }
  };
  
  module.exports = opts;