"use strict";

const httpStatus = require("http-status-codes");
const logger = require("../../../logger/logger");
const { v4: uuidv4 } = require('uuid');

class TodoController {
  constructor(interActor, eventEmitter) {
    this.interActor = interActor;
    this.eventEmitter = eventEmitter;
  }

  async handleTodoRequest(request, reply) {
    logger.info("create new TODO");
    const todoRequest = {
      id: uuidv4(),
      title: request.body.title,
      description: request.body.description,
      createdDate: Date.now(),
    };
    try {
      const todo = await this.interActor.createTodo(todoRequest);
      reply.code(httpStatus.OK).send({ todoRequest: todoRequest });
    } catch (err) {
      logger.error(
        `Error occur while handling todo request ${err.stack}`
      );
      const statusCode =
        err.code ||
        err.statusCode ||
        err.response?.status ||
        httpStatus.INTERNAL_SERVER_ERROR;
      let message = err.code || err.response?.data.error.message || err.message;
      if (message === "Already created an identity for this user") {
        message = `This identity ${identityRequest.identifier} already created in NDID`;
      }
      reply.code(statusCode).send({ message: message });
    }
  }

  async handleGetTodos(request, reply) {
    logger.info('Get todos');
    try {
      const todos = await this.interActor.getTodos();
      if (todos) {
        todos.errorMessage = todos.errorMessage === null ?  undefined : todos.errorMessage;
        reply
          .code(httpStatus.OK)
          .send(
            {
              data: todos,
            }
          );
      } else {
        reply
          .code(httpStatus.NOT_FOUND)
          .send(
            {
              message: `Not found todos`
            }
          );
      }

    } catch (err) {
      logger.error(`Error occur while handling get todos`);
      const statusCode = err.statusCode || err.response?.status || httpStatus.INTERNAL_SERVER_ERROR;
      const message = err.code || err.response?.data.error.message || err.message;
      reply
        .code(statusCode)
        .send({ message: message});
    }
  }

  async handleGetTodoById(request, reply){
    logger.info('Get Todo by id');
    const id = request.params.id;
    try {
      const todo = await this.interActor.getTodoById(id);
      if (todo) {
        todo.errorMessage = todo.errorMessage === null ?  undefined : todo.errorMessage;
        reply
          .code(httpStatus.OK)
          .send(
            {
              data: todo,
            }
          );
      } else {
        reply
          .code(httpStatus.NOT_FOUND)
          .send(
            {
              message: `Not found todo by id`
            }
          );
      }

    } catch (err) {
      logger.error(`Error occur while handling get todo ${err.stack}`);
      const statusCode = err.statusCode || err.response?.status || httpStatus.INTERNAL_SERVER_ERROR;
      const message = err.code || err.response?.data.error.message || err.message;
      reply
        .code(statusCode)
        .send({ message: message});
    }
  }

  async handleUpdateTodoRequest(request, reply){
    logger.info("create new TODO");
    const todoRequest = {
      id: request.params.id,
      title: request.body.title,
      description: request.body.description,
      createdDate: Date.now(),
    };
    try {
      const todo = await this.interActor.updateTodo(todoRequest);
      reply.code(httpStatus.OK).send({ todoRequest: todoRequest });
    } catch (err) {
      logger.error(
        `Error occur while handling update todo ${err.stack}`
      );
      const statusCode =
        err.code ||
        err.statusCode ||
        err.response?.status ||
        httpStatus.INTERNAL_SERVER_ERROR;
      let message = err.code || err.response?.data.error.message || err.message;
      reply.code(statusCode).send({ message: message });
    }
  }

  async handleDeleteTodo(request, reply){
    logger.info("delete TODO");
    const id = request.params.id;
    try {
      const todo = await this.interActor.deleteTodo(id);
      reply.code(httpStatus.OK).send({ id: id });
    } catch (err) {
      logger.error(
        `Error occur while handling delete todo ${err.stack}`
      );
      const statusCode =
        err.code ||
        err.statusCode ||
        err.response?.status ||
        httpStatus.INTERNAL_SERVER_ERROR;
      let message = err.code || err.response?.data.error.message || err.message;
      reply.code(statusCode).send({ message: message });
    }
  }

}

module.exports = TodoController;
