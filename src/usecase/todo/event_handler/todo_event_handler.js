
class TodoEventHandler {
  constructor(todoInterActor) {
    this.todoInterActor = todoInterActor;
  }

  async createTodoHandler(data) {
    logger.debug(`Handling create_todo callback, data ${JSON.stringify(data)}`);
    await this.todoInterActor.createTodo(data);
  }

  async updateTodoHandler(data) {
    logger.debug(`Handling create_todo callback, data ${JSON.stringify(data)}`);
    await this.todoInterActor.createTodo(data);
  }
}

module.exports = TodoEventHandler;
