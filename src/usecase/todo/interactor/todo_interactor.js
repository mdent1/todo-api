const Todo = require("../../../entity/todo");
const TodoUpdate = require("../../../entity/todo_update");
const logger = require("../../../logger/logger");

class TodoInterActor {
  constructor(dataAdapter) {
    this.dataAdapter = dataAdapter;
  }

  async createTodo(callbackData) {
    logger.debug(`Create new TODO data: ${JSON.stringify(callbackData)}`);
    try {
        const toCreateTodo = new Todo(
          callbackData.id,
          callbackData.title,
          callbackData.description,
          callbackData.createdDate,
        );
        await this.dataAdapter.createTodo(toCreateTodo);
    } catch (e) {
      logger.error(e.message);
    }
  }

  async getTodos(){
    logger.debug(`Get todos`);
    try {
      const todos = await this.dataAdapter.getTodos();
      console.log(todos)
      if (todos) {
        return todos;
      } else {
        return null;
      }
    } catch (err) {
      logger.debug(`Error while getting todos`);
      throw err;
    }
  }

  async getTodoById(id){
    logger.debug(`Get todo by reference id ${id}`);
    try {
      const todo = await this.dataAdapter.getTodoById(id);
      if (todo) {
        return todo;
      } else {
        logger.warn(`Cannot find todo by id ${id}`);
        return null;
      }
    } catch (err) {
      logger.debug(`Error while getting todo by id ${id}`);
      throw err;
    }
  }

  async updateTodo(callbackData){
    logger.debug(`Update TODO data: ${JSON.stringify(callbackData)}`);
    try {
        const toUpdateTodo = new TodoUpdate(
          callbackData.title,
          callbackData.description,
          callbackData.createdDate,
        );
        await this.dataAdapter.updateTodo(toUpdateTodo, callbackData.id);
    } catch (e) {
      logger.error(e.message);
    }
  }

  async deleteTodo(id){
    logger.debug(`Delete TODO id: ${id}`);
    try {
        await this.dataAdapter.deleteTodo(id);
    } catch (e) {
      logger.error(e.message);
    }
  }
}

module.exports = TodoInterActor;