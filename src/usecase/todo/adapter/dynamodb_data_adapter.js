const logger = require("../../../logger/logger");
const config = require("../../../config/config");
const Todo = require("../../../entity/todo");

class DynamodbDataAdapter {
  constructor(documentClient) {
    this.documentClient = documentClient;
  }

  async createTodo(todo) {
    logger.debug(`Creating Todo ${todo}`);
    const params = {
      TableName: config.DYNAMODB.TABLE.TODO,
      Item: {
        id: todo.id,
        title: todo.title,
        description: todo.description,
        createdDate: todo.createdDate,
      },
    };
    try {
      await this.documentClient.put(params).promise();
    } catch (err) {
      logger.error(err.stack);
      throw err;
    }
  }

  async getTodos() {
    logger.debug(`Getting todos`);
    const getParams = {
      TableName:  config.DYNAMODB.TABLE.TODO,
    };
    try {
      const result = await this.documentClient.scan(getParams).promise();
      if (result["Items"]) {
        // const todo = result["Items"];
        const todo = result["Items"].map(item => {
          return new Todo(
            item.id,
            item.title,
            item.description,
            item.createdDate,
          );
        });
        return todo;
      }
      return null;
    } catch (e) {
      logger.error(
        `Error while getting todos, ${e.stack}`
      );
      throw e;
    }
  }
  async getTodoById(id){
    logger.debug(`Getting todo from id ${id}`);
    const getParams = {
      TableName: config.DYNAMODB.TABLE.TODO,
      Key: {'id': id}
    };
    try {
      const result = await this.documentClient.get(getParams).promise();
      if (result['Item']) {
        const item = result['Item'];
        return new Todo(
          item.id,
          item.title,
          item.description,
          item.createdDate,
        );
      }
      return null;
    } catch (err) {
      logger.error(`Error while getting todo status from database, id ${id}, ${err.stack}`);
      throw err;
    }
  }

  async updateTodo(todo, id) {
    logger.debug(`Updating Todo ${todo}`);
    logger.debug(`id : ${todo.id}`)
    const {
      updateExpression,
      expressionAttributeNames,
      expressionAttributeValues
    } = this.getUpdateExpression(todo);
    const params = {
      TableName: config.DYNAMODB.TABLE.TODO,
      Key: {
        'id': id
      },
      UpdateExpression: updateExpression,
      ExpressionAttributeNames: expressionAttributeNames,
      ExpressionAttributeValues: expressionAttributeValues,
      ReturnValues: 'UPDATED_NEW'
    };
    try {
      await this.documentClient.update(params).promise();
    } catch (err) {
      logger.error(`Error occur during update todo reference ${todo.id}, ${err.stack}`);
      throw err;
    }
  }

  async deleteTodo(id){
    logger.debug(`Getting todo from id ${id}`);
    const params = {
      TableName: config.DYNAMODB.TABLE.TODO,
      Key: {'id': id}
    };
    try {
      await this.documentClient.delete(params).promise();
    } catch (err) {
      logger.error(`Error occur during update todo reference ${todo.id}, ${err.stack}`);
      throw err;
    }
  }

  getUpdateExpression(updateBody) {
    let updateExpression = 'set';
    let expressionAttributeNames = {};
    let expressionAttributeValues = {};
    for (const property in updateBody) {
      updateExpression += ` #${property} = :${property},`;
      expressionAttributeNames [`#${property}`] = property;
      expressionAttributeValues [`:${property}`] = updateBody[property];
    }
    updateExpression = updateExpression.slice(0, -1);
    return { updateExpression, expressionAttributeNames, expressionAttributeValues };
  }
  
}

module.exports = DynamodbDataAdapter;
