'use strict';

const dotenv = require('dotenv');
dotenv.config();
const API_SERVER_ADDRESS = process.env.API_SERVER_ADDRESS || 'http://localhost:8100';
const isDevEnv = process.env.NODE_ENV === 'development';

module.exports = {

  AWS_ACCESS_KEY_ID: process.env.AWS_ACCESS_KEY_ID,
  AWS_SECRET_ACCESS_KEY: process.env.AWS_SECRET_ACCESS_KEY,
  AWS_REGION: 'ap-southeast-1',
  DYNAMODB: {
    TABLE: {
      TODO: 'todo_tonnam',
    }
  },
  PAGINATION_LIMIT: process.env.PAGINATION_LIMIT || 10
};
