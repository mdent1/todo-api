class TodoUpdate {
  constructor(title, description, createdDate) {
    this.title = title;
    this.description = description;
    this.createdDate = createdDate;
  }
}

module.exports = TodoUpdate;
