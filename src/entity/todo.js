class Todo {
  constructor(id, title, description, createdDate) {
    this.id = id;
    this.title = title;
    this.description = description;
    this.createdDate = createdDate;
  }
}

module.exports = Todo;
