const pino = require('pino');
const dotenv = require('dotenv');
dotenv.config();
const env = process.env.NODE_ENV;

const logger = pino({
  name: 'idp-service',
  level: process.env.LOG_LEVEL || 'debug',
  timestamp: () => {
    return ', "time":"' + new Date().toISOString() + '"';
    // 2017-07-31T10:31:14.000Z
  },
  prettyPrint: env !== 'production',
});

module.exports = logger;
